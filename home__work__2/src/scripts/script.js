const headerBlack = document.querySelector("#header-black");
const headerBlackTop = headerBlack.offsetTop;
const widthWindow = window.innerWidth;

window.addEventListener('scroll', function () {
    if (widthWindow >= 1200) {
        headerBlack.classList.add("container__black");
    }
    if (window.pageYOffset === 0) {
        headerBlack.classList.remove("container__black");
    }
});


const burgerMenuIcon = document.querySelector(".burger-menu__icon");
const burgerTenuText = document.querySelector(".burger-menu__text");

    burgerMenuIcon.addEventListener('click', function () {

        burgerTenuText.classList.toggle("active");
    })




